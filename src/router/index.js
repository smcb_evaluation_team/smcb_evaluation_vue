import Vue from 'vue'
import Router from 'vue-router'

// Containers
const DefaultContainer = () => import('@/containers/DefaultContainer')

// Views
const Dashboard = () => import('@/views/Dashboard')

// Views - Add
const Faculty = () => import('@/views/add/Faculty')
const Staff = () => import('@/views/add/Staff')
const Student = () => import('@/views/add/Student')
const Semester = () => import('@/views/add/Semester')
const Subject = () => import('@/views/add/Subject')
const Batch = () => import('@/views/add/Batch')

// Views - View
const Faculties = () => import('@/views/view/Faculties')
const Staffs = () => import('@/views/view/Staffs')
const Students = () => import('@/views/view/Students')
const Semesters = () => import('@/views/view/Semesters')
const Subjects = () => import('@/views/view/Subjects')
const AssignedF2C = () => import('@/views/view/AssignedF2C')
const AssignedS2S = () => import('@/views/view/AssignedS2S')

// Views - Assign
const Subjects2Faculty = () => import('@/views/assign/Subjects2Faculty')
const Subjects2Batch = () => import('@/views/assign/Subjects2Batch')
const CustomAssign = () => import('@/views/assign/CustomAssign')

// Views - Settings
const ShiftStudent = () => import('@/views/settings/ShiftStudent')

// Views - Reports
const FacultyEvaluation = () => import('@/views/reports/FacultyEvaluation')
const StaffEvaluation = () => import('@/views/reports/StaffEvaluation')
const NonSubStudents = () => import('@/views/reports/NonSubStudents')

const app = document.createElement('div');
app.setAttribute('data-app', true);
document.body.append(app);


Vue.use(Router)

export default new Router({
  mode: 'hash', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: '/dashboard',
      name: 'Home',
      component: DefaultContainer,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: 'add',
          redirect: '/add/faculty',
          name: 'Add New',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'faculty',
              name: 'Faculty',
              component: Faculty
            },
            {
              path: 'staff',
              name: 'Staff',
              component: Staff
            },
            {
              path: 'student',
              name: 'Student',
              component: Student
            },
            {
              path: 'semester',
              name: 'Semester',
              component: Semester
            },
            {
              path: 'subject',
              name: 'Subject',
              component: Subject
            },
            {
              path: 'batch',
              name: 'Batch',
              component: Batch
            }
          ]
        },
        {
          path: 'view',
          redirect: '/view/faculties',
          name: 'View',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'faculties',
              name: 'Faculties',
              component: Faculties
            },
            {
              path: 'staffs',
              name: 'Staffs',
              component: Staffs
            },
            {
              path: 'students',
              name: 'Students',
              component: Students
            },
            {
              path: 'semesters',
              name: 'Semesters',
              component: Semesters
            },
            {
              path: 'subjects',
              name: 'Subjects',
              component: Subjects
            },
            {
              path: 'assigned-faculties-with-subjects',
              name: 'Assigned Faculties with Subjects',
              component: AssignedF2C
            },
            {
              path: 'assigned-subjects-with-students',
              name: 'Assigned Subjects with Students',
              component: AssignedS2S
            }
          ]
        },
        {
          path: 'assign',
          redirect: '/assign/subjects-to-faculty',
          name: 'Assign',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'subjects-to-faculty',
              name: 'Subjects to Faculty',
              component: Subjects2Faculty
            },
            {
              path: 'subjects-to-batch',
              name: 'Subjects to Batch',
              component: Subjects2Batch
            },
            {
              path: 'custom-assign',
              name: 'Custom Assign',
              component: CustomAssign
            }
          ]
        },
        {
          path: 'settings',
          redirect: '/settings/shift-student',
          name: 'Settings',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'shift-student',
              name: 'Shift Student',
              component: ShiftStudent
            }
          ]
        }
        ,
        {
          path: 'reports',
          redirect: '/reports/faculty-evaluation',
          name: 'Reports',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'faculty-evaluation',
              name: 'Faculty Evaluation',
              component: FacultyEvaluation
            },
            {
              path: 'staff-evaluation',
              name: 'Staff Evaluation',
              component: StaffEvaluation
            },
            {
              path: 'non-submmited-students',
              name: 'Non Submitted Students',
              component: NonSubStudents
            }
          ]
        }
      ]
    }
  ]
})
