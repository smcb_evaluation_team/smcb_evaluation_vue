export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
      badge: {
        variant: 'primary',
        // text: 'NEW'
      }
    },
    // {
    //   title: true,
    //   name: 'Admin',
    //   class: '',
    //   wrapper: {
    //     element: '',
    //     attributes: {}
    //   }
    // },
    {
      name: 'Add New',
      url: '/theme/colors',
      icon: 'fa fa-plus',
      children: [
         {
          name: 'Faculty',
          url: '/add/faculty',
          icon: 'fa fa-user'
        },
        {
          name: 'Staff',
          url: '/add/staff',
          icon: 'fa fa-child'
        },
        {
          name: 'Student',
          url: '/add/student',
          icon: 'fa fa-users'
        },
        {
          name: 'Semester',
          url: '/add/semester',
          icon: 'fa fa-bookmark'
        },
        {
          name: 'Subject',
          url: '/add/subject',
          icon: 'fa fa-book'
        },
        {
          name: 'Batch',
          url: '/add/batch',
          icon: 'fa fa-folder-open'
        }
      ]
    },
    {
      name: 'View',
      url: '/theme/colors',
      icon: 'fa fa-th-large',
      children: [
         {
          name: 'Faculties',
          url: '/view/faculties',
          icon: 'fa fa-user'
        },
        {
          name: 'Staffs',
          url: '/view/staffs',
          icon: 'fa fa-child'
        },
        {
          name: 'Students',
          url: '/view/students',
          icon: 'fa fa-users'
        },
        {
          name: 'Semesters',
          url: '/view/semesters',
          icon: 'fa fa-users'
        },
        {
          name: 'Subjects',
          url: '/view/subjects',
          icon: 'fa fa-book'
        },
        {
          name: 'Assigned Faculties with Subjects',
          url: '/view/assigned-faculties-with-subjects',
          icon: 'fa fa-users'
        },
        {
          name: 'Assigned Subjects with Students (Batch)',
          url: '/view/assigned-subjects-with-students',
          icon: 'fa fa-users'
        }
      ]
    },
    {
      name: 'Assign',
      url: '/theme/colors',
      icon: 'fa fa-check-square-o',
      children: [
         {
          name: 'Subjects to Faculty',
          url: '/assign/subjects-to-faculty',
          icon: 'fa fa-user'
        },
        {
          name: 'Subjects to Batch',
          url: '/assign/subjects-to-batch',
          icon: 'fa fa-child'
        },
        {
          name: 'Custom Assign',
          url: '/assign/custom-assign',
          icon: 'fa fa-users'
        }
      ]
    },
    {
      name: 'Settings',
      url: '/theme/colors',
      icon: 'fa fa-gears',
      children: [
        {
          name: 'Shift Student',
          url: '/settings/shift-student',
          icon: 'fa fa-users'
        }
      ]
    },
    {
      name: 'Reports',
      url: '/theme/colors',
      icon: 'fa fa-file-pdf-o',
      children: [
         {
          name: 'Faculty Evaluation',
          url: '/reports/faculty-evaluation',
          icon: 'fa fa-user'
        },
        {
          name: 'Staff Evaluation',
          url: '/reports/staff-evaluation',
          icon: 'fa fa-user'
        },
        {
          name: 'Non Submitted Student',
          url: '/reports/non-submmited-students',
          icon: 'fa fa-users'
        }
      ]
    }
  ]
}
